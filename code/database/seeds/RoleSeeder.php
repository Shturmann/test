<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['id' => 1, 'role' => 'admin'],
            ['id' => 5, 'role' => 'moderator'],
            ['id' => 10, 'role' => 'user'],
        ]);
    }
}
