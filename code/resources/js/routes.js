import VueRouter from 'vue-router';


let routes = [
    {
        path: '/',
        name: 'Posts',
        component: require('./views/posts').default
    },
    {
        path: '/profile',
        name: 'Profile',
        component: require('./views/profile').default,
        props: true
    }
];


export default new VueRouter({
    base: '/posts',
    mode: 'history',
    routes,
    linkActiveClass: 'active'
});
