<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function (){
    Route::post('user/profile','UserController@profile');
    Route::post('user/ban','UserController@ban')->middleware('is.admin');

    Route::get('posts', 'PostController@index');
    Route::post('post/store', 'PostController@store');
    Route::post('post/{id}/delete', 'PostController@destroy')->middleware('is.admin');
    Route::post('post/{id}/edit', 'PostController@edit')->middleware('is.admin');
});
