<?php

namespace Tests\Feature;

use App\Post;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\WithStubUser;

class PostsTest extends TestCase
{
    use DatabaseTransactions, WithStubUser;

    public function test_index_authentication()
    {
        $this->assertAuthenticationRequired('/posts');
        $this->assertAuthenticationRequired('/post/store');
        $this->assertAuthenticationRequired('/post/delete');
    }

    public function testIndex()
    {
        $user = $this->createStubUser();
        $response = $this->actingAs($user)->get('/posts');

        $response->assertStatus(200);;
    }

    public function testAuthUserCanStoreNewPost()
    {
        $this->actingAs($this->createStubUser());

        $this->get('/post/store', ['text' => 'test'])
            ->assertStatus(200);
    }

    public function testStoreInvalidPost()
    {
        $this->actingAs($this->createStubUser());

        $this->postJson('/post/store', ['text' => ''])
            ->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['text']]);
    }

    public function testAuthUserCanEditPost()
    {
        $post = $this->createPost();

        $this->post("/post/{$post->id}/edit")
            ->assertStatus(200);

        $this->put("/post/{$post->id}", ['text' => 'updated text']);
    }

    public function testAuthUserCanDeletePost()
    {
        $post = $this->createPost();

        $this->post("/post/{$post->id}/delete")
            ->assertRedirect('/posts')
            ->assertSessionHas('deleted', $post->id);
    }

    private function createPost($authenticated = true)
    {
        $post = factory(Post::class)->create();

        if ($authenticated) {
            $this->actingAs($this->createStubUser());
        }

        return $post;
    }
}
